# Naive Bayes Classifier Competency Framework(s)

The Naive Bayes Classifier Competency Framework maps the competencies of the domain of Naive Bayes Classifiers in order to provide the necessary information for an upcoming adaptive learning environment. It is part of the research within the [KIWI project](https://www.htw-berlin.de/forschung/online-forschungskatalog/projekte/projekt/?eid=3125) at the HTW Berlin.

The competency framework is in ongoing development and comes in two forms:

1. [Miro digital whiteboard](https://miro.com/app/board/uXjVPR95Qe0=/) (early iterations and graphical overview)
2. Modelled as linked data in [CaSS](https://www.cassproject.org/) and exported as JSON in [this repository](https://gitlab.com/adaptive-learning-engine/naive-bayes/lr/-/tree/main/competencies). Currently only contains a subset of the competencies.

This repository is primarily maintained to guarantee the continued validity of the link provided in the corresponding publication [Designing Granular Competency Frameworks for Adaptive Learning on the Example of Naïve Bayes Classifiers](https://dl.gi.de/items/c063ab04-9d6c-49c2-bb51-32b6b565bc6d).